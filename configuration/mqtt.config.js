const mqtt = require('mqtt');
require('dotenv').config()
const mqttService = require('../services/mqtt.service');
const client = mqtt.connect(process.env.MQTT_BROKER_URL, {});

const mqttConnection = () => {
    try {
        client.on('connect', () => {
            client.subscribe(process.env.MQTT_TOPIC_1);
        });

        client.on('message', (tp, msg) => {
            console.log('DEBUG::Received MQTT msg: ', msg.toString());
            try {
                const data = JSON.parse(msg.toString());
                mqttService.pushDeviceData(data).then(() => {
                    console.log('DEBUG::Updated device: ', data);
                }).catch((err) => {
                    console.error('DEBUG::Error updating device: ', err);
                });
            } catch (err) {
                console.log(err);
            }
        })
    } catch (err) {
        console.log('ERROR::', err);
    }
}

module.exports = {
    mqttConnection,
    client
};