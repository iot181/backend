const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const homesModel = Schema({
    name: {
        type: String,
        default: ''
    },
    rooms: [
        {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'rooms'
        }
    ],
    owner: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'users'
    }
}, {collection: 'homes'})

module.exports = mongoose.model('homes', homesModel)