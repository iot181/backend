const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const usersModel = new Schema({
    email: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        default: ''
    },
    fullName: {
        type: String,
        default: ''
    },
    home: [{
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'homes'
    }],
    createAt: {
        type: Date,
        default: Date.now
    }
}, {collection: 'users'})


module.exports = mongoose.model('users', usersModel)
