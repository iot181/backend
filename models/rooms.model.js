const mongoose = require('mongoose')


const roomsModel = mongoose.Schema({
    name: {
        type: String,
        default: ''
    },
    type: {
        type: String,
        default: ''
    },
    address: {
        type: String,
        default: ''
    },
    home: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'homes'
    },
    devices: [
        {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'devices'
        }
    ]
}, {collection: 'rooms'})

module.exports = mongoose.model('rooms', roomsModel)