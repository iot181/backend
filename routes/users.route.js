const express = require("express");
const userController = require('../controllers/users.controller');
const authenticate = require('../middleware/authenticate');
const router = express.Router();

router.post('/register', userController.register);
router.post('/login', userController.login);
router.get('/info', authenticate.checkAccessToken, userController.info);
router.post('/update-password', authenticate.checkAccessToken, userController.updatePassword);
router.post('/update-info', authenticate.checkAccessToken, userController.updateInfo);

module.exports = router;