const express = require("express");
const authenticate = require("../middleware/authenticate");
const homeController = require('../controllers/homes.controller');
const router = express.Router();

//home
router.post('/create', authenticate.checkAccessToken, homeController.create);
router.get('/list', authenticate.checkAccessToken, homeController.getList);
router.get('/:id', authenticate.checkAccessToken, homeController.getOne);
router.put('/:id', authenticate.checkAccessToken, homeController.updateOne);
router.delete('/:id', authenticate.checkAccessToken, homeController.deleteOne);

//room
router.post('/room/create', authenticate.checkAccessToken, homeController.addRoom);
router.get('/room/list', authenticate.checkAccessToken, homeController.getRoomList);
router.get('/room/:id', authenticate.checkAccessToken, homeController.getRoomOne);
router.put('/room/:id', authenticate.checkAccessToken, homeController.updateRoom);
router.delete('/room/:id', authenticate.checkAccessToken, homeController.deleteRoom);

//device
router.post('/room/device/create', authenticate.checkAccessToken, homeController.addDevice);
router.get('/room/device/list', authenticate.checkAccessToken, homeController.getDeviceList);
router.get('/room/device/:id', authenticate.checkAccessToken, homeController.getDeviceOne);
router.put('/room/device/:id', authenticate.checkAccessToken, homeController.updateDevice);
router.delete('/room/device/:id', authenticate.checkAccessToken, homeController.deleteDevice);


module.exports = router;