const Devices = require('../models/devices.model');
const {updateDeviceControl} = require("./mqtt.service");

exports.pushDeviceData = async (data) => {
    try {
        if (data['sensorData'] && data['sensorData'].length > 0) { //reset data
            const ids = data['sensorData'].map(item => item.id);
            const uniqueIds = [...new Set(ids)];

            const totalData = {};
            for (let i = 0; i < uniqueIds.length; i++) {
                totalData[`${uniqueIds[i]}`] = [];
                for (let j = 0; j < data['sensorData'].length; j++) {
                    if (uniqueIds[i] === data['sensorData'][j].id) {
                        totalData[`${uniqueIds[i]}`].push(data['sensorData'][j]);
                    }
                }
            }

            console.log('DEBUG::pushDeviceData ', totalData);
            for (let i = 0; i < uniqueIds.length; i++) {
                await Devices.findByIdAndUpdate(
                    uniqueIds[i]
                    , {
                        $set: {
                            data: totalData[`${uniqueIds[i]}`]
                        }
                    }
                );
            }


        } else if (data['lampData'] && data['lampData'].length > 0) {
            for (let i = 0; i < data['lampData'].length; i++) {
                console.log("DEBUG::update control", data['lampData'][i])
                await Devices.findByIdAndUpdate(
                    data['lampData'][i].id
                    , {
                        $set: {
                            control: {
                                status: data['lampData'][i].value == 1 ? true : false,
                            }
                        }
                    }
                );
            }
        }

        return true;
    } catch (err) {
        console.log(err);
    }
}

exports.updateDeviceControl = async ({deviceId, control}) => {
    try {
        const device = await Devices.findByIdAndUpdate(
            deviceId
            , {
                $set: {
                    control: control
                }
            }
        );

        return device;
    } catch (err) {
        console.log(err);
    }
}

exports.controlDevice = (mqttClient, deviceId, control) => {
    try {
        console.log('DEBUG::CONTROL', deviceId, control);
        mqttClient.publish(process.env.MQTT_TOPIC_2, JSON.stringify({
            id: deviceId,
            status: control.status
        }));

    } catch (err) {
        console.log(err);
    }
}