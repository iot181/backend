const Users = require('../models/users.model');
const bcrypt = require("bcrypt");

exports.getUserById = async (id) => {
    return await Users.findOne({_id: id}).exec();
}

exports.genPassword = async (password) => {
    const salt = await bcrypt.genSalt(16);
    return bcrypt.hash(password, salt);
}

exports.getUserByEmailOrUsername = async (data) => {
    const {email, username} = data;
    if (email) {
        return Users.findOne({email: email}).exec();
    }

    if (username) {
        return Users.findOne({username: username}).exec();
    }
}

