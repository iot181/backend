const Homes = require('../models/homes.model');
const Users = require("../models/users.model");
const Rooms = require('../models/rooms.model');
const Devices = require('../models/devices.model');
const {client} = require('../configuration/mqtt.config');
const mqttService = require('../services/mqtt.service');
const {CustomError} = require("../helpers/custom-error");
const {return_codes, messages, error_codes} = require("../helpers/constant");
const mongoose = require("mongoose");

exports.checkHomePermission = async (homeId, user) => {
    // check id is valid ObjectId
    if (!mongoose.Types.ObjectId.isValid(homeId)) {
        throw new CustomError(return_codes.CLIENT_ERROR, messages.OBJECT_ID_INVALID, error_codes.OBJECT_ID_INVALID);
    }

    const home = await this.getOneById(homeId);
    if (!home) {
        throw new CustomError(return_codes.CLIENT_ERROR, messages.HOME_NOT_EXIST, error_codes.HOME_NOT_EXIST);
    }

    // check home is owner by user or not
    if (home.owner.toString() !== user._id.toString()) {
        throw new CustomError(return_codes.CLIENT_ERROR, messages.HOME_NOT_OWNED_BY_USER, error_codes.HOME_NOT_OWNED_BY_USER);
    }

    return home;
}

exports.checkRoomPermission = async (roomId, user) => {
    // check id is valid ObjectId
    if (!mongoose.Types.ObjectId.isValid(roomId)) {
        throw new CustomError(return_codes.CLIENT_ERROR, messages.OBJECT_ID_INVALID, error_codes.OBJECT_ID_INVALID);
    }

    // get room by id
    const room = await this.getRoom(roomId);

    if (!room) {
        throw new CustomError(return_codes.CLIENT_ERROR, messages.ROOM_NOT_EXIST, error_codes.ROOM_NOT_EXIST);
    }

    // check room is owned by user or not
    if (room.home.owner.toString() !== user._id.toString()) {
        throw new CustomError(return_codes.CLIENT_ERROR, messages.ROOM_NOT_OWNED_BY_USER, error_codes.ROOM_NOT_OWNED_BY_USER);
    }

    return room;
}

exports.checkDevicePermission = async (deviceId, user) => {
    // check id is valid ObjectId
    if (!mongoose.Types.ObjectId.isValid(deviceId)) {
        throw new CustomError(return_codes.CLIENT_ERROR, messages.OBJECT_ID_INVALID, error_codes.OBJECT_ID_INVALID);
    }

    // get device by id
    const device = await this.getDevice(deviceId);

    if (!device) {
        throw new CustomError(return_codes.CLIENT_ERROR, messages.DEVICE_NOT_EXIST, error_codes.DEVICE_NOT_EXIST);
    }

    // check device is owned by user or not
    if (device.room.home.owner.toString() !== user._id.toString()) {
        throw new CustomError(return_codes.CLIENT_ERROR, messages.DEVICE_NOT_OWNED_BY_USER, error_codes.DEVICE_NOT_OWNED_BY_USER);
    }

    return device;
}

exports.createOne = async (data, user) => {
    // create home with owner is user
    const home = new Homes(data);
    await home.save();

    await Users.updateOne({
        _id: user._id
    }, {
        $push: {
            homes: home._id
        }
    });

    return home;
}
exports.getListHome = async (params) => {
    const keySearch = {};
    if (params.name) {
        keySearch.name = {
            $regex: params.name,
            $options: 'i'
        }
    }

    if (params.owner) {
        keySearch.owner = params.owner;
    }

    const homes = await Homes.find(keySearch)
        .skip((params.page - 1) * params.limit)
        .limit(params.limit)
        .exec();

    const total = await Homes.countDocuments(keySearch);
    // count number of page
    const totalPage = Math.ceil(total / params.limit);

    return {
        data: homes,
        pagination: {
            total,
            totalPage
        }
    }
}

exports.getOneById = async (id) => {
    const home = await Homes.findById(id).populate('rooms').exec();
    return home;
}

exports.updateOne = async (id, data) => {
    const keyUpdate = {};
    if (data.name) {
        keyUpdate.name = data.name;
    }
    // update home properties by id
    const home = await Homes.findByIdAndUpdate(id, {
        $set: keyUpdate
    }, {
        new: true
    });

    return home;
}

// create deleteOne function to delete home by id
exports.deleteOne = async (id) => {
    const home = await Homes.findByIdAndDelete(id);

    // delete all rooms of this home
    await Rooms.deleteMany({
        home: home._id
    });

    // delete all devices of all rooms above
    await Devices.deleteMany({
        room: {
            $in: home.rooms.map(room => room._id)
        }
    });

    return home;
}

exports.addRoom = async (data, home) => {
    const room = new Rooms({
        ...data,
        home: home._id
    });
    await room.save();

    // add room to home
    await Homes.updateOne({
        _id: home._id
    }, {
        $push: {
            rooms: room._id
        }
    });

    return room;
}

exports.getRoom = async (roomId) => {
    // get room by id and populate home and devices
    const room = await Rooms.findById(roomId)
        .populate('home')
        .populate('devices')
        .exec();

    return room;
}

exports.getListRoom = async (params) => {
    const keySearch = {};
    if (params.name) {
        keySearch.name = {
            $regex: params.name,
            $options: 'i'
        }
    }

    if (params.type) {
        keySearch.type = {
            $regex: params.type,
            $options: 'i'
        }
    }

    if (params.address) {
        keySearch.address = {
            $regex: params.address,
            $options: 'i'
        }
    }

    if (params.homeId) {
        keySearch.home = params.homeId;
    }

    const rooms = await Rooms.find(keySearch)
        .skip((params.page - 1) * params.limit)
        .limit(params.limit)
        .exec();

    const total = await Rooms.countDocuments(keySearch);
    // count number of page
    const totalPage = Math.ceil(total / params.limit);

    //return data
    return {
        data: rooms,
        pagination: {
            total,
            totalPage
        }
    }
}

exports.updateRoom = async (roomId, data) => {
    const keyUpdate = {};
    if (data.name) {
        keyUpdate.name = data.name;
    }
    if (data.type) {
        keyUpdate.type = data.type;
    }
    if (data.address) {
        keyUpdate.address = data.address;
    }

    // update room properties by id
    const room = await Rooms.findOneAndUpdate({
        _id: roomId,
    }, {
        $set: keyUpdate
    }, {
        new: true
    });

    return room;
}

exports.deleteRoom = async (roomId) => {
    const room = await Rooms.findByIdAndDelete(roomId);

    // delete all devices of this room
    await Devices.deleteMany({
        room: room._id
    });

    // pull room from home
    await Homes.updateOne({
        _id: room.home
    }, {
        $pull: {
            rooms: {
                $in: [room._id]
            }
        }
    });

    return room;
}

exports.addDevice = async (data, roomId) => {
    // create device
    const device = new Devices({
        ...data,
        room: roomId
    });
    await device.save();

    // add device to room
    await Rooms.updateOne({
        _id: roomId
    }, {
        $push: {
            devices: device._id
        }
    });

    return device;
}

exports.getDevice = async (deviceId) => {
    // get device and room of this device and home of this room
    const device = await Devices.findById(deviceId).populate({
        path: 'room',
        populate: {
            path: 'home'
        }
    }).exec();

    return device;
}

exports.getListDevice = async (params) => {
    const keySearch = {};
    if (params.name) {
        keySearch.deviceName = {
            $regex: params.name,
            $options: 'i'
        }
    }

    if (params.roomId) {
        keySearch.room = params.roomId;
    }

    if (params.type) {
        keySearch.deviceType = params.type;
    }

    if (params.status) {
        keySearch.status = params.status;
    }

    const devices = await Devices.find(keySearch)
        .skip((params.page - 1) * params.limit)
        .limit(params.limit)
        .exec();

    const total = await Devices.countDocuments(keySearch);
    // count number of page
    const totalPage = Math.ceil(total / params.limit);

    //return data
    return {
        data: devices,
        pagination: {
            total,
            totalPage
        }
    }
}

exports.updateDevice = async (deviceId, data) => {
    // push data into device.data if exist data
    if (data.data) {
        await Devices.updateOne({
            _id: deviceId
        }, {
            $push: {
                data: {
                    value: data.data.value,
                }
            }
        });

    }
    // update control of device if exist control
    if (data.control) {
        //TODO: call MQTT to update status of physical device
        mqttService.controlDevice(client, deviceId, data.control);
        await Devices.findOneAndUpdate({
            _id: deviceId
        }, {
            $set: {
                control: {
                    status: data.control.status,
                    mode: data.control.mode,
                    direction: data.control.direction,
                    speed: data.control.speed,
                    intensity: data.control.intensity
                }
            }
        });
    }

    // update device type and name if each of them exist
    const keyUpdate = {};
    if (data.deviceName) {
        keyUpdate.deviceName = data.deviceName;
    }
    if (data.deviceType) {
        keyUpdate.deviceType = data.deviceName;
    }

    const device = await Devices.findOneAndUpdate({
        _id: deviceId
    }, {
        $set: keyUpdate
    }, {
        new: true
    });

    return device;
}

exports.deleteDevice = async (deviceId) => {
    // delete device by id
    const device = await Devices.findByIdAndDelete(deviceId);

    // remove device by id in room
    await Rooms.updateOne({
        _id: device.room
    }, {
        $pull: {
            devices: {
                $in: [deviceId]
            }
        }
    });

    return device;
}