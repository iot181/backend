const {handleCatchError, CustomError} = require("../helpers/custom-error");
const {messages, error_codes, return_codes} = require("../helpers/constant");
const userService = require("../services/users.service");
const jwt = require("jsonwebtoken");
require('dotenv').config()

exports.checkAccessToken = async (req, res, next) => {
    try {
        if (
            !req.headers
            || !req.headers.authorization
        ) {
            throw new CustomError(return_codes.CLIENT_ERROR, messages.HEADER_AUTHORIZATION_ERROR, error_codes.HEADER_AUTHORIZATION_ERROR);
        }

        let authorization = req.headers.authorization;
        let arr = authorization.split(" ");

        if (!arr[1]) {
            throw new CustomError(return_codes.TOKEN_ERROR, messages.ACCESS_TOKEN_INVALID, error_codes.TOKEN_INVALID);
        }

        await jwt.verify(arr[1], process.env.JWT_SECRET);

        let data = jwt.decode(arr[1]);
        if (!data) {
            throw new CustomError(return_codes.TOKEN_ERROR, messages.ACCESS_TOKEN_INVALID, error_codes.TOKEN_INVALID);
        }
        if (Date.now() >= data.exp * 1000) {
            throw new CustomError(return_codes.TOKEN_ERROR, messages.ACCESS_TOKEN_EXPIRED, error_codes.ACCESS_TOKEN_EXPIRED);
        }

        let user = await userService.getUserById(data.id);
        if (!user) {
            throw new CustomError(return_codes.CLIENT_ERROR, messages.USER_WITH_ID_NOT_EXIST, error_codes.TOKEN_INVALID);
        }

        // pass data through next middleware
        req.data = data;
        req.user = user;

        next();
    } catch (error) {
        handleCatchError(error, res);
    }
}
