var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors')
const mongoConnection = require("./configuration/database.config");
const {mqttConnection} = require("./configuration/mqtt.config");
const userRoutes = require('./routes/users.route')
const homeRoutes = require('./routes/home.route')
const index = require('./routes/index.route')
require('dotenv').config()

var app = express();
mongoConnection().then(() => {
    console.log("Database connected!");
}).catch((error) => {
    console.log(error);
});
mqttConnection();

// handle error
process.on('uncaughtException', err => {
    console.error('There was an uncaught error', err);
});
process.on('unhandledRejection', (reason, promise) => {
    console.log('Unhandled Rejection at:', reason)
})


app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use('/', index);
app.use('/user', userRoutes);
app.use('/home', homeRoutes);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    console.log(err)
    res.json('error');
});

module.exports = app;
