const Users = require('../models/users.model')
const {handleCatchError, CustomError} = require("../helpers/custom-error");
const {return_codes, error_codes, messages} = require("../helpers/constant");
const userService = require("../services/users.service");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
require('dotenv').config();

exports.register = async (req, res, next) => {
    try {
        const {email, username, password, fullName, phone} = req.body;

        const user = await  userService.getUserByEmailOrUsername({email, username});

        if (user) {
            throw new CustomError(return_codes.CLIENT_ERROR, error_codes.USERNAME_OR_EMAIL_EXIST, messages.USERNAME_OR_EMAIL_EXIST);
        } else {
            const newUser = new Users({
                email: email,
                username: username,
                password: await userService.genPassword(password),
                fullName: fullName,
                phone: phone,
            });

            await newUser.save();
            const savedUser = newUser.toObject();
            delete savedUser.password;
            res.status(200).json({
                success: true,
                data: savedUser,
            })
        }

    } catch (error) {
        handleCatchError(error, res);
    }
}

exports.login = async (req, res, next) => {
    try {
        const {email, username, password} = req.body;

        if (!password || (!email && !username)) {
            throw new CustomError(return_codes.CLIENT_ERROR, messages.DATA_INVALID, error_codes.DATA_INVALID);
        }

        const user = await userService.getUserByEmailOrUsername({email, username});
        const comparePass = await bcrypt.compare(password, user.password);

        if (!comparePass){
            throw new CustomError(return_codes.CLIENT_ERROR, messages.PASSWORD_INVALID, error_codes.PASSWORD_INVALID);
        }

        const accessToken = await jwt.sign({id: user.id, email: user.email, username: user.username}, process.env.JWT_SECRET);

        delete user.password;
        res.status(200).json({
            success: true,
            data: {
                accessToken,
                user: user
            }
        })
    } catch (error) {
        handleCatchError(error, res);
    }
}

exports.info =  async (req, res, next) => {
    try {
        const user = req.user.toObject();
        delete user.password;

        res.status(200).json({
           success: true,
           data: user
        });
    } catch (error) {
        handleCatchError(error, res);
    }
}

exports.updatePassword = async (req, res, next) => {
    try {
        const {oldPassword, newPassword} = req.body;

        const user = req.user;
        if (!oldPassword || !newPassword) {
            throw new CustomError(return_codes.CLIENT_ERROR, messages.DATA_INVALID, error_codes.DATA_INVALID);
        }

        const comparePass = await bcrypt.compare(oldPassword, user.password);
        if (comparePass) {
            user.password = await userService.genPassword(newPassword);

            const result = await Users.updateOne({_id: user._id}, {password: user.password});
            res.status(200).json({
                success: true,
                data: result
            });
        } else {
            throw new CustomError(return_codes.CLIENT_ERROR, messages.PASSWORD_INVALID, error_codes.PASSWORD_INVALID);
        }
    } catch (error) {
        handleCatchError(error, res);
    }
}

exports.updateInfo = async (req, res, next) => {
    try {
        const {fullName, phone} = req.body;
        const user = req.user;

        if (fullName) {
            user.fullName = fullName;
        }
        if (phone) {
            user.phone = phone;
        }
        console.log(user)

        const result = await Users.updateOne({_id: user._id}, {...user.toObject()});
        res.status(200).json({
            success: true,
            data: result,
        });
    } catch (error) {
        console.log(error)
        handleCatchError(error, res);
    }
}