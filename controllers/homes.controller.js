const Homes = require('../models/homes.model')
const {handleCatchError, CustomError} = require("../helpers/custom-error");
const {messages, error_codes, return_codes} = require("../helpers/constant");
const Users = require('../models/users.model')
const homesService = require('../services/homes.service');
const mongoose = require("mongoose");

//general
exports.create = async (req, res, next) => {
    try {
        const user = req.user;
        const {name} = req.body;
        if (!name) {
            throw new CustomError(return_codes.CLIENT_ERROR, messages.DATA_INVALID, error_codes.DATA_INVALID);
        }

        const data = {
            name,
            owner: user._id,
        };

        const home = await homesService.createOne(data, user);

        res.status(201).json({
            success: true,
            data: home,
        });
    } catch (error) {
        handleCatchError(error, res);
    }

}

exports.getList = async (req, res, next) => {
    // get list of homes from database, pagination them by params in body
    // and return them to client
    try {
        const user = req.user;
        const {page, limit, name} = req.query;

        // check page or limit is valid
        if (!page || !limit || page < 1) {
            throw new CustomError(return_codes.CLIENT_ERROR, messages.MISSING_PAGINATION_PARAMS, error_codes.MISSING_PAGINATION_PARAMS);
        }

        const params = {
            owner: user._id,
            page: page,
            limit: limit,
            name: name
        };

        // call service to get list of homes
        const result = await homesService.getListHome(params);

        res.status(200).json({
            success: true,
            ...result,
        });
    } catch (error) {
        handleCatchError(error, res);
    }
}

exports.getOne = async (req, res, next) => {
    try {
        const user = req.user;
        const {id} = req.params;
        if (!id) {
            throw new CustomError(return_codes.CLIENT_ERROR, messages.DATA_INVALID, error_codes.DATA_INVALID);
        }

        const home = await homesService.getOneById(id);

        await homesService.checkHomePermission(id, user);

        res.status(200).json({
            success: true,
            data: home,
        });
    } catch (error) {
        console.log(error)
        handleCatchError(error, res);
    }
}

exports.updateOne = async (req, res, next) => {
    try {
        const {id} = req.params;
        const user = req.user;

        await homesService.checkHomePermission(id, user);

        const result = await homesService.updateOne(id, req.body);

        res.status(200).json({
            success: true,
            data: result
        });
    } catch (error) {
        console.log(error);
        handleCatchError(error, res);
    }
}

exports.deleteOne = async (req, res, next) => {
    try {
        const {id} = req.params;
        const user = req.user;

        await homesService.checkHomePermission(id, user);

        const result = await homesService.deleteOne(id);

        res.status(200).json({
            success: true,
            data: result
        });
    } catch (error) {
        handleCatchError(error, res);
    }
}

// for room
exports.getRoomOne = async (req, res, next) => {
    // get room by id in params
    try {
        const {id} = req.params;
        const user = req.user;

        await homesService.checkRoomPermission(id, user);

        const result = await homesService.getRoom(id);

        res.status(200).json({
            success: true,
            data: result
        });
    } catch (error) {
        console.log(error)
        handleCatchError(error, res);
    }

}

exports.getRoomList = async (req, res, next) => {
    // get list home and pagination them by params in body
    // and return them to client
    try {
        const user = req.user;
        const {page, limit, name, homeId} = req.query;

        // check page or limit is valid
        if (!page || !limit || page < 1) {
            throw new CustomError(return_codes.CLIENT_ERROR, messages.MISSING_PAGINATION_PARAMS, error_codes.MISSING_PAGINATION_PARAMS);
        }

        const params = {
            owner: user._id,
            page: page,
            limit: limit,
            name: name,
            homeId: homeId
        };

        // call service to get list of homes
        const result = await homesService.getListRoom(params);

        res.status(200).json({
            success: true,
            ...result,
        });
    } catch (error) {
        handleCatchError(error, res);
    }
}

exports.addRoom = async (req, res, next) => {
    try {
        const {homeId} = req.query;
        const user = req.user;

        if (!homeId) {
            throw new CustomError(return_codes.CLIENT_ERROR, messages.MISSING_HOME_ID, error_codes.MISSING_HOME_ID);
        }

        const home = await homesService.checkHomePermission(homeId, user);
        const room = await homesService.addRoom(req.body, home);

        res.status(200).json({
            success: true,
            data: room
        })
    } catch (error) {
        console.log(error)
        handleCatchError(error, res);
    }
}

exports.updateRoom = async (req, res, next) => {
    // update room data by id and data in body
    try {
        const {id} = req.params;
        const user = req.user;

        await homesService.checkRoomPermission(id, user);

        const result = await homesService.updateRoom(id, req.body);

        res.status(200).json({
            success: true,
            data: result
        });
    }  catch (error) {
        handleCatchError(error, res);
    }
}

exports.deleteRoom = async (req, res, next) => {
    // delete room by id in params
    try {
        const {id} = req.params;
        const user = req.user;

        await homesService.checkRoomPermission(id, user);

        const result = await homesService.deleteRoom(id);

        res.status(200).json({
            success: true,
            data: result
        });
    }  catch (error) {
        handleCatchError(error, res);
    }
}

//for device
exports.getDeviceOne = async (req, res, next) => {
    // get detail data of devices by id in params
    try {
        const {id} = req.params;
        const user = req.user;

        await homesService.checkDevicePermission(id, user);

        const result = await homesService.getDevice(id);

        res.status(200).json({
            success: true,
            data: result
        });
    } catch (error) {
        handleCatchError(error, res);
    }
}

exports.getDeviceList = async (req, res, next) => {
    // get list of homes from database, pagination them by params in body
    // and return them to client
    try {
        const user = req.user;
        const {page, limit, name, type, roomId, status} = req.query;

        // check page or limit is valid
        if (!page || !limit || page < 1) {
            throw new CustomError(return_codes.CLIENT_ERROR, messages.MISSING_PAGINATION_PARAMS, error_codes.MISSING_PAGINATION_PARAMS);
        }

        const params = {
            owner: user._id,
            page: page,
            limit: limit,
            name: name,
            type: type,
            roomId: roomId,
            status: status
        };

        // call service to get list of homes
        const result = await homesService.getListDevice(params);

        res.status(200).json({
            success: true,
            ...result,
        });
    } catch (error) {
        handleCatchError(error, res);
    }
}

exports.addDevice = async (req, res, next) => {
    // add device in a room by roomId in query
    try {
        const {roomId} = req.query;
        const user = req.user;

        if (!roomId) {
            throw new CustomError(return_codes.CLIENT_ERROR, messages.MISSING_ROOM_ID, error_codes.MISSING_ROOM_ID);
        }

        await homesService.checkRoomPermission(roomId, user);

        const device = await homesService.addDevice(req.body, roomId);

        res.status(200).json({
            success: true,
            data: device
        })
    } catch (error) {
        console.log(error)
        handleCatchError(error, res);
    }
}

// use to update device name or push data or update control properties
exports.updateDevice = async (req, res, next) => {
    // add data into device by id and check permission
    try {
        const {id} = req.params;
        const user = req.user;

        await homesService.checkDevicePermission(id, user);

        const result = await homesService.updateDevice(id, req.body);

        res.status(200).json({
            success: true,
            data: result
        });
    } catch (error) {
        handleCatchError(error, res);
    }
}

exports.deleteDevice = async (req, res, next) => {
    // delete device by id in params
    try {
        const {id} = req.params;
        const user = req.user;

        await homesService.checkDevicePermission(id, user);

        const result = await homesService.deleteDevice(id);

        res.status(200).json({
            success: true,
            data: result
        });
    } catch (error) {
        handleCatchError(error, res);
    }
}
